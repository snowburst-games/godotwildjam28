// Scene data: contains references to all Stage scenes within the game - for simplicity so we don't have to update path in each script.

using System;

public class SceneData
{
    public enum Stage
    {
        MainMenu,
        World,
        Scores
    }

    public enum Scene
    {
        RoomOffice,
        RoomKitchen,
        RoomHR
    }

    public readonly System.Collections.Generic.Dictionary<Stage,string> Stages 
        = new System.Collections.Generic.Dictionary<Stage, string>()
    {
        {Stage.MainMenu, "res://Stages/MainMenu/StageMainMenu.tscn"},
        {Stage.World, "res://Stages/World/StageWorld.tscn"},
        {Stage.Scores, "res://Stages/Scores/StageScores.tscn"}
    };

    public static System.Collections.Generic.Dictionary<Scene,string> Scenes 
        = new System.Collections.Generic.Dictionary<Scene, string>()
    {
        // {Scene.NetState, "res://Global/NetState/NetState.tscn"},
        // {Scene.Lobby, "res://Utils/Network/Lobby/Lobby.tscn"},
        // {Scene.Player, "res://Entities/Characters/Player/Player.tscn"},
        // {Scene.Creature, "res://Entities/Creature/Creature.tscn"},
        {Scene.RoomOffice, "res://Levels/Interior/RoomOffice.tscn"},
        {Scene.RoomKitchen, "res://Levels/Interior/RoomKitchen.tscn"},
        {Scene.RoomHR, "res://Levels/Interior/HR/RoomHR.tscn"}
    };
}
