using Godot;
using System;

public class EggSplatShell : Node2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private float _speed = 300;
	private float _accel = 1.05f;
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _PhysicsProcess(float delta)
	{
		_speed *= _accel;
		Position = new Vector2(Position.x, Position.y + (_speed*delta));

		if (Position.y - GetNode<Sprite>("Sprite").Texture.GetSize().y > GetViewportRect().Size.y)
		{
			GD.Print(Position.y);
			GD.Print(GetViewportRect().Size.y);
			QueueFree();
		}
	}
}
