using Godot;
using System;

public class GridBG : TextureRect
{
	private int _numCols = 0;
	private int _numRows = 0;
	private int _cellSize = 0;
	private int _lineThickness = 0;
	private float _borderSize = 0;

	private TextureRect _cornerUL;
	private TextureRect _sideU;
	private Label _lblTitle;
	public override void _Ready()
	{
		_cornerUL = GetNode<TextureRect>("CornerUL");
		_sideU = GetNode<TextureRect>("SideU");
		_lblTitle = GetNode<Label>("LblTitle");
	}

	public void RenderGrid(int numCols, int numRows, int cellSize, int lineThickness, string title, float borderSize = 45)
	{
		_numCols = numCols;
		_numRows = numRows;
		_cellSize = cellSize;
		_lineThickness = lineThickness;
		_lblTitle.Text = title;
		_borderSize = borderSize;

		SetBorder();
		Update();
	}

	private void SetBorder()
	{
		_cornerUL.RectSize = new Vector2(_borderSize, _borderSize);
		_sideU.RectSize = new Vector2(_borderSize, _borderSize);
		SetCorners();
		SetSides();
		_lblTitle.RectPosition = new Vector2(_sideU.RectSize.x/2 - (_lblTitle.RectSize.x/2)*_lblTitle.RectScale.x, -_sideU.RectSize.y);
	}
	private void SetCorners()
	{
		TextureRect cornerBL = (TextureRect)_cornerUL.Duplicate();
		TextureRect cornerUR = (TextureRect)_cornerUL.Duplicate();
		TextureRect cornerBR = (TextureRect)_cornerUL.Duplicate();
		_cornerUL.RectPosition = new Vector2(-_cornerUL.RectSize);
		AddChild(cornerBL);
		cornerBL.RectPosition = new Vector2(-cornerBL.RectSize.x, _numRows*_cellSize);
		cornerBL.FlipV = true;
		AddChild(cornerUR);
		cornerUR.RectPosition = new Vector2(_numCols*_cellSize , -cornerBL.RectSize.y);
		cornerUR.FlipH = true;
		AddChild(cornerBR);
		cornerBR.RectPosition = new Vector2(_numCols*_cellSize , _numRows*_cellSize);
		cornerBR.FlipV = true;
		cornerBR.FlipH = true;

		
	}

	private void SetSides()
	{
		_sideU.RectPivotOffset = _sideU.RectSize/2f;
		TextureRect sideD = (TextureRect)_sideU.Duplicate();
		TextureRect sideL = (TextureRect)_sideU.Duplicate();
		TextureRect sideR = (TextureRect)_sideU.Duplicate();
		_sideU.RectPosition = new Vector2(0, -_sideU.RectSize.y);
		_sideU.RectSize = new Vector2(_numCols*_cellSize, _sideU.RectSize.y);

		AddChild(sideD);
		sideD.RectPosition = new Vector2(0, _numRows*_cellSize);
		sideD.RectSize = new Vector2(_numCols*_cellSize, sideD.RectSize.y);
		sideD.FlipV = true;


		AddChild(sideL);
		sideL.RectPosition = new Vector2(-sideL.RectSize.x, 0);
		sideL.RectSize = new Vector2(_numRows*_cellSize, sideL.RectSize.y);
		sideL.FlipV = true;
		sideL.RectRotation = 90;


		AddChild(sideR);
		sideR.RectPosition = new Vector2(_numCols*_cellSize, 0);
		sideR.RectSize = new Vector2(_numRows*_cellSize, sideR.RectSize.y);
		sideR.FlipV = false;
		sideR.RectRotation = 90;
	}

	public override void _Draw()
	{
		// loop through each column (x)
		for (int col = 0; col <= _numCols; col++)
		{
			// draw a line from each column downwards (i.e. to the max rows * cell height)
			DrawLine(new Vector2((col)*_cellSize, 0), new Vector2((col)*_cellSize, (_numRows*_cellSize)), new Color(0,0,0,0.05f), _lineThickness, true);

			
		}// loop through each row (y)
		for (int row = 0; row <= _numRows; row++)
		{
			// draw a line from each row to the right (i.e. to the max cols * cell width)
			DrawLine(new Vector2(0, (row)*_cellSize), new Vector2(_numCols * _cellSize, (row)*_cellSize), new Color(0,0,0,0.05f), _lineThickness, true);
		}
	}
}
