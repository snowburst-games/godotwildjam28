// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};


	public enum ButtonSounds {
		ClickStart
	};

	public enum EggSounds {
		Boing,
		Splat,
		Cheer1,
		Cheer2,
		Cheer3,
		Cheer4,
		Cheer5,
		EggInAir1,
		EggInAir2
	};

	public enum MainMenuSounds {
		Music,
		Intro
	};
	public enum WorldSounds {
		Music,
		TimesUp,
		HighScore
	};

	public enum SlingshotSounds {
		SlingShoot1,
		SlingShoot2,
		Stretch		
	}

	public enum TargetSounds {
		Cheer1,
		Cheer2,
		Cheer3,
		Cheer4,
		Cheer5
	}

	public static Dictionary<ButtonSounds, string> ButtonSoundPaths = new Dictionary<ButtonSounds, string>()
	{
		{ButtonSounds.ClickStart, "res://Interface/Sounds/Click.wav"}
	};

	public static Dictionary<MainMenuSounds, string> MainMenuSoundPaths = new Dictionary<MainMenuSounds, string>()
	{
		{MainMenuSounds.Music, "res://Stages/MainMenu/Music/Egg_menuv0.2.ogg"},
		{MainMenuSounds.Intro, "res://Stages/MainMenu/Sound/TheEggiesIntrov0.2.wav"},
	};

	public static Dictionary<WorldSounds, string> WorldSoundPaths = new Dictionary<WorldSounds, string>()
	{
		{WorldSounds.Music, "res://Stages/World/Music/EggMain_Orchestra_speed_v0.2.ogg"},
		{WorldSounds.TimesUp, "res://Stages/World/Sound/Timesup.wav"},
		{WorldSounds.HighScore, "res://Stages/World/Sound/WinnerFanfair.wav"}
	};

	public static Dictionary<TargetSounds, string> TargetSoundPaths = new Dictionary<TargetSounds, string>()
	{
		
		{TargetSounds.Cheer1, "res://Props/Target/Sound/Pickup_Score.wav"},
		{TargetSounds.Cheer2, "res://Props/Target/Sound/Pickup_Score.wav"},
		{TargetSounds.Cheer3, "res://Props/Target/Sound/Pickup_Score.wav"},
		{TargetSounds.Cheer4, "res://Props/Target/Sound/Pickup_Score.wav"},
		{TargetSounds.Cheer5, "res://Props/Target/Sound/Pickup_Score.wav"}
	};

	public static Dictionary<SlingshotSounds, string> SlingshotSoundPaths = new Dictionary<SlingshotSounds, string>()
	{
		
		{SlingshotSounds.SlingShoot1, "res://Props/Slingshot/Sound/shoot.wav"},
		{SlingshotSounds.SlingShoot2, "res://Props/Slingshot/Sound/shoot2.wav"},
		{SlingshotSounds.Stretch, "res://Props/Slingshot/Sound/elastic.wav"}
	};
	public static Dictionary<EggSounds, string> EggSoundPaths = new Dictionary<EggSounds, string>()
	{
		
		{EggSounds.Boing, "res://Props/Egg/Sound/Jump8_v0.2.wav"},
		{EggSounds.Splat, "res://Props/Egg/Sound/Splat3.wav"},
		{EggSounds.Cheer1, "res://Props/Egg/Sound/Happy1.wav"},
		{EggSounds.Cheer2, "res://Props/Egg/Sound/Happy2.wav"},
		{EggSounds.Cheer3, "res://Props/Egg/Sound/Happy3.wav"},
		{EggSounds.Cheer4, "res://Props/Egg/Sound/Happy4.wav"},
		{EggSounds.Cheer5, "res://Props/Egg/Sound/Happy5.wav"},
		{EggSounds.EggInAir1, "res://Props/Egg/Sound/EggInAir.wav"},
		{EggSounds.EggInAir2, "res://Props/Egg/Sound/EggInAir2.wav"}
	};


	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
