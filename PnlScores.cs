using Godot;
using System;

public class PnlScores : Panel
{
	private CntScores _cntScores;

	public override void _Ready()
	{
		_cntScores = (CntScores) GetNode("CntScores");
		Start();
	}

	public void Start()
	{
		_cntScores.Modulate = new Color(1,1,1,0);
		GetNode<AnimationPlayer>("Anim").Play("Loading");
		CallDeferred(nameof(ShowScores));
	}

	private void ShowScores()
	{
		_cntScores.ShowScores();
		GetNode<Timer>("DumbTimer").Start();
	}
	private void OnBtnClosePressed()
	{
		Visible = false;
		// _cntScores.ClearRenderedScores();
	}

}
