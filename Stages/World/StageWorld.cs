// next
// egg art and other available
// music
// sfx

using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{	
	public Dictionary<AudioData.WorldSounds, AudioStream> WorldSoundPaths {get; set;} = AudioData.LoadedSounds<AudioData.WorldSounds>(AudioData.WorldSoundPaths);
	public AudioStreamPlayer MusicPlayer {get; set;}
	public AudioStreamPlayer SoundPlayer {get; set;}
	private Slingshot _slingshot;
	private Line2D _leftLine;
	private Line2D _rightLine;
	private Line2D _eggLine;
	private Node2D _resetPoints;
	private Vector2 _leftLinePointPos;
	private Vector2 _rightLinePointPos;
	private Vector2 _centrePos;
	private Vector2 _aimPos;
	private Egg _egg;
	private PackedScene _eggScn;
	private PackedScene _eggSplatScn;
	private PackedScene _eggSplatShellScn;
	private PackedScene _lblFloatScoreScn;
	private Random random = new Random();

	private enum EggColour {
		Blue, Green, Mint, Purple, Red, Yellow
	}

	private Dictionary<EggColour, string> _eggColourTexPaths = new Dictionary<EggColour, string>() {
		{EggColour.Blue, "res://Props/Egg/Art/EGG_BLUE_201220/EGG_BLUE201220.png.resized.png"},
		{EggColour.Green, "res://Props/Egg/Art/EGG_GREEN_201220/EGG_GREEN201220.png.resized.png"},
		{EggColour.Mint, "res://Props/Egg/Art/EGG_MINT_201220/EGG_MINT201220.png.resized.png"},
		{EggColour.Purple, "res://Props/Egg/Art/EGG_PURPLE_201220/EGG_PURPLE201220.png.resized.png"},
		{EggColour.Red, "res://Props/Egg/Art/EGG_RED_201220/EGG_RED201220.png.resized.png"},
		{EggColour.Yellow, "res://Props/Egg/Art/EGG_YELLOW_201220/EGG_YELLOW201220.png.resized.png"},
	};

	public int Score = 0;

	public override void _Ready()
	{
		base._Ready();
		MusicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		SoundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		MusicPlayer.VolumeDb = -20;
		GetNode<AnimationPlayer>("MusicAnim").Play("Start");
		// AudioHandler.PlaySound(_musicPlayer, _worldSoundPaths[AudioData.WorldSounds.Music], AudioData.SoundBus.Music);
		GetNode<Panel>("HUD/PnlMenu").Visible = false;
		_slingshot = GetNode<Slingshot>("Slingshot");
		_centrePos = _slingshot.GetNode<Position2D>("Centre").GlobalPosition;
		_aimPos = _slingshot.GetNode<Position2D>("Aim").GlobalPosition;

		_eggScn = GD.Load<PackedScene>("res://Props/Egg/Egg.tscn");
		_eggSplatScn = GD.Load<PackedScene>("res://Doodads/EggSplat/EggSplat.tscn");
		_eggSplatShellScn = GD.Load<PackedScene>("res://Doodads/EggSplat/EggSplatShell.tscn");
		_lblFloatScoreScn = GD.Load<PackedScene>("res://Common/Nodes/FloatScoreLabel/LblFloatScore.tscn");

		// _egg = GetNode<Egg>("Egg");
		// _egg.BallDragged+=this.SetLinePoint;
		// _egg.StoppedDrag+=this.ResetLineToOrigin;
		// _egg.CentrePos = _centrePos;
		// _egg.EggDied+=this.OnEggDied;

		_leftLine = _slingshot.GetNode<Line2D>("SLeft/Point/Line");
		_rightLine = _slingshot.GetNode<Line2D>("SRight/Point/Line");
		_eggLine = _slingshot.GetNode<Line2D>("SEgg/Point/Line");
		_leftLinePointPos = _slingshot.GetNode<Position2D>("SLeft/Point").GlobalPosition;
		_rightLinePointPos = _slingshot.GetNode<Position2D>("SRight/Point").GlobalPosition;
		_resetPoints = _slingshot.GetNode<Node2D>("ResetPoints");

		GetNode<TargetHandler>("TargetHandler").TargetHit+=this.OnTargetHit;
		// GetNode<PnlScores>("HUD/PnlScores").Start();
		ResetLineToOrigin();
	}

	private Texture GetEggColourTexture(EggColour colour)
	{
		return (Texture)GD.Load(_eggColourTexPaths[colour]);
	}

	public void OnEggDied(Egg egg)
	{
		if (_gameEnded)
		{
			return;
		}
		// also make splat
		GetNode<TargetHandler>("TargetHandler").OnEggLanded(egg); // tell the targethandler that the egg did splat
		// GenerateEgg();
		Array eggColourValues = Enum.GetValues(typeof(EggColour));
		_currentEggColour = (EggColour)eggColourValues.GetValue(random.Next(eggColourValues.Length));
		GetNode<Sprite>("FakeEgg").Texture = GetEggColourTexture(_currentEggColour);
		GetNode<AnimationPlayer>("Anim").Play("Start");

	}

	public void OnTargetHit(int score, Vector2 pos, bool timeBonus)
	{
		if (_gameEnded)
		{
			return;
		}
		// update score here
		GD.Print(score);
		Score+= score;

		if (timeBonus)
		{
			float timeLeft = GetNode<Timer>("GameTimer").TimeLeft;
			GetNode<Timer>("GameTimer").Start(timeLeft+5);
		}

		GetNode<Label>("HUD/LblScore").Text = "Score: " + Score;
		if (score > 0)
		{
			LblFloatScore _lblFloatScore = (LblFloatScore) _lblFloatScoreScn.Instance();
			_lblFloatScore.Text = score.ToString();
			if (timeBonus)
			{
				_lblFloatScore.Text += "\n+5 sec";
			}
			_lblFloatScore.RectPosition = pos;
			AddChild(_lblFloatScore);
		}

		if (score == 0)
		{
			GenerateEggSplat(pos);
		}
	}

	private void GenerateEggSplat(Vector2 pos)
	{
		Node2D eggSplat = (Node2D) _eggSplatScn.Instance();
		AddChild(eggSplat);
		eggSplat.Position = pos;

		Node2D eggSplatShell = (Node2D) _eggSplatShellScn.Instance();
		// set the egg shell colour
		eggSplatShell.GetNode<Sprite>("Sprite").Texture = GetEggColourTexture(_currentEggColour);
		AddChild(eggSplatShell);
		eggSplatShell.Position = pos;
	}

	private void GenerateEgg()
	{
		Egg egg = (Egg) _eggScn.Instance();
		AddChild(egg);
		egg.GetNode<Sprite>("Sprite").Texture = GetEggColourTexture(_currentEggColour);
		_egg = egg;
		egg.BallDragged+=this.SetLinePoint;
		egg.BallDragged+=_slingshot.OnSlingshotStretch;
		egg.StoppedDrag+=this.ResetLineToOrigin;
		egg.StoppedDrag+=_slingshot.OnSlingshotShot;
		egg.EggLanded+=this.OnEggDied;
		egg.CentrePos = _centrePos;
		egg.AimPos = _aimPos;
		egg.GlobalPosition = new Vector2(_aimPos.x, _aimPos.y+20);
	}

	private void SetLinePoint()
	{
		_leftLine.SetPointPosition(1, _egg.GetNode<Position2D>("PosLeft").GlobalPosition - _leftLine.GlobalPosition);
		_rightLine.SetPointPosition(1, _egg.GetNode<Position2D>("PosRight").GlobalPosition - _rightLine.GlobalPosition);
		_eggLine.SetPointPosition(0, _egg.GetNode<Position2D>("PosLeft").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(1, _egg.GetNode<Position2D>("PosCentreLeft1").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(2, _egg.GetNode<Position2D>("PosCentreLeft").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(3, _egg.GetNode<Position2D>("PosCentreRight").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(4, _egg.GetNode<Position2D>("PosCentreRight1").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(5, _egg.GetNode<Position2D>("PosRight").GlobalPosition - _eggLine.GlobalPosition);
		// GD.Print(_leftLine.GetPointPosition(1));
		_leftLine.ZIndex = 1;
		if (_leftLine.GetPointPosition(1).x < -10)
		{
			_leftLine.ZIndex = -1;
		}
		_rightLine.ZIndex = 1;
		if (_rightLine.GetPointPosition(1).x > 10)
		{
			_rightLine.ZIndex = -1;
		}
	}

	private void ResetLineToOrigin()
	{
		_leftLine.SetPointPosition(1, _resetPoints.GetNode<Position2D>("1").GlobalPosition - _leftLine.GlobalPosition);
		_rightLine.SetPointPosition(1, _resetPoints.GetNode<Position2D>("6").GlobalPosition - _rightLine.GlobalPosition);
		_eggLine.SetPointPosition(0, _resetPoints.GetNode<Position2D>("1").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(1, _resetPoints.GetNode<Position2D>("2").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(2, _resetPoints.GetNode<Position2D>("3").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(3, _resetPoints.GetNode<Position2D>("4").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(4, _resetPoints.GetNode<Position2D>("5").GlobalPosition - _eggLine.GlobalPosition);
		_eggLine.SetPointPosition(5, _resetPoints.GetNode<Position2D>("6").GlobalPosition - _eggLine.GlobalPosition);
		_leftLine.ZIndex = 1;
		_rightLine.ZIndex = 1;

	}

	private void OnHUDAnimFinished(string anim_name)
	{
		if (anim_name == "WorldStart")
		{

			

			Array eggColourValues = Enum.GetValues(typeof(EggColour));
			// egg.GetNode<Sprite>("Sprite").Texture = GetEggColourTexture((EggColour)eggColourValues.GetValue(random.Next(eggColourValues.Length)));
			_currentEggColour = (EggColour)eggColourValues.GetValue(random.Next(eggColourValues.Length));
			GetNode<Sprite>("FakeEgg").Texture = GetEggColourTexture(_currentEggColour);
			GetNode<AnimationPlayer>("Anim").Play("Start");
		}
	}

	private EggColour _currentEggColour;

	private bool _newGame = true;

	private void OnWorldAnimFinished(String anim_name)
	{
		if (_gameEnded)
		{
			return;
		}
		if (anim_name == "Start")
		{
			GenerateEgg();
			SetLinePoint();
			if (_newGame)
			{
				_newGame = false;
				GetNode<Timer>("GameTimer").Start();
			}
		}
	}

	private bool _gameEnded = false;

	private void OnGameTimerTimeout()
	{
		_gameEnded = true;
		GetNode<Timer>("TargetHandler/SpawnTimer").Stop();
		GetNode<Button>("HUD/BtnMenu").Disabled = true;
		GetNode<ScoreManagerMono>("HUD/ScoreManagerMono").StartScoreSaver(Score, 10);
		

	}	

	private void _on_score_saver_high_score()
	{
		AudioHandler.PlaySound(SoundPlayer, WorldSoundPaths[AudioData.WorldSounds.HighScore], AudioData.SoundBus.Effects);
	}


	private void _on_score_saver_no_high_score()
	{
		AudioHandler.PlaySound(SoundPlayer, WorldSoundPaths[AudioData.WorldSounds.TimesUp], AudioData.SoundBus.Effects);
	}

	private void _on_score_saver_finished()
	{
		ShowEndMenu();
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (!GetNode<Timer>("GameTimer").IsStopped())
		{
			GetNode<Label>("HUD/LblTimer").Text = string.Format("Time left: {0}",Math.Floor(GetNode<Timer>("GameTimer").TimeLeft));
		}
		
	}

	private void OnBtnMenuPressed()
	{
		GetNode<Panel>("HUD/PnlMenu").Visible = true;
		GetTree().Paused = true;
	}


	private void OnBtnResumePressed()
	{
		GetNode<Panel>("HUD/PnlMenu").Visible = false;
		GetTree().Paused = false;
	}


	private void OnBtnMainMenuPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu, new Dictionary<string, object>() {{"Fullscreen", OS.WindowFullscreen}});
		GetTree().Paused = false;
	}

	private void OnBtnRestartPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
		GetTree().Paused = false;
	}

	private void _on_BtnLeaderboard_pressed()
	{
		GetNode<ScoreManagerMono>("HUD/ScoreManagerMono").ShowAndRefreshLeaderboard(10);
	}

	public void ShowEndMenu()
	{
		// GetNode<ScoreManagerMono>("HUD/ScoreManagerMono").RefreshLeaderboard(10);
		// GetNode<PnlScores>("HUD/PnlScores").Start();
		GetNode<Button>("HUD/PnlMenu/BtnResume").Visible = false;
		GetNode<Button>("HUD/PnlMenu/BtnLeaderboard").Visible = true;
		// GetNode<Button>("HUD/PnlMenu/BtnMainMenu").RectPosition = new Vector2(67.5f, 156);
		// GetNode<Button>("HUD/PnlMenu/BtnRestart").RectPosition = new Vector2(67, 71.5f);
		GetNode<Label>("HUD/PnlMenu/LblTitle").Text = "TIME'S UP";
		GetNode<Panel>("HUD/PnlMenu").Visible = true;
	}

}



