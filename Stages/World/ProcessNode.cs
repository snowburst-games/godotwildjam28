using Godot;
using System;

public class ProcessNode : Node
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Node _silentWolfMono;
	public StageWorld StageWorld {get; set;}

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
		_silentWolfMono = GetNode("/root/SilentWolfMono");
	}

	public bool TryingToGetScorePosition {get; set;}= false;
	public bool TryingToSubmitScore {get; set;} = false;

	public override void _Process(float delta)
	{
		base._Process(delta);

		if (_silentWolfMono == null || StageWorld == null)
		{
			return;
		}

		bool scoresReceived = (bool)_silentWolfMono.Get("scores_received_bool");
		bool scoresPositionReceived = (bool)_silentWolfMono.Get("score_position_received_bool");
		bool scoreSubmitted = (bool)_silentWolfMono.Get("score_submitted_bool");
		// GD.Print(TryingToSubmitScore, scoreSubmitted);
		// if (TryingToGetScorePosition && scoresPositionReceived)
		// {
		// 	int maxLines = (int)_silentWolfMono.Get("max_scores");
		// 	Single scorePosition = (Single)_silentWolfMono.Get("lastScorePosition");
		// 	// GD.Print(scorePosition.GetType());
		// 	// int scorePosition = int.Parse((string)silentWolfMono.Get("lastScorePosition"));
		// 	if (scorePosition <= maxLines && StageWorld.Score > 0)
		// 	{
		// 		StageWorld.GetNode<Panel>("HUD/PnlHighScore").Visible = true;
		// 		AudioHandler.PlaySound(StageWorld.SoundPlayer, StageWorld.WorldSoundPaths[AudioData.WorldSounds.HighScore], AudioData.SoundBus.Effects);
		// 	}
		// 	else
		// 	{
		// 		StageWorld.ShowEndMenu();
		// 		AudioHandler.PlaySound(StageWorld.SoundPlayer, StageWorld.WorldSoundPaths[AudioData.WorldSounds.TimesUp], AudioData.SoundBus.Effects);
		// 	}
		// 	_silentWolfMono.Set("score_position_received_bool", false);
		// 	TryingToGetScorePosition = false;
		// }
		// if (TryingToGetScorePosition)
		// {
		// 	StageWorld.GetNode<Panel>("HUD/PnlHighScore").Visible = true;
		// 	TryingToGetScorePosition = false;
		// }
		
	}
	private void OnDumbTimerTimeout()
	{

		// if (TryingToGetScorePosition)
		// {
		// 	int maxLines = (int)_silentWolfMono.Get("max_scores");
		// 	Single scorePosition = (Single)_silentWolfMono.Get("lastScorePosition");
		// 	// GD.Print(scorePosition.GetType());
		// 	// int scorePosition = int.Parse((string)silentWolfMono.Get("lastScorePosition"));
		// 	if (scorePosition <= maxLines && StageWorld.Score > 0)
		// 	{
		// 		StageWorld.GetNode<Panel>("HUD/PnlHighScore").Visible = true;
		// 		AudioHandler.PlaySound(StageWorld.SoundPlayer, StageWorld.WorldSoundPaths[AudioData.WorldSounds.HighScore], AudioData.SoundBus.Effects);
		// 	}
		// 	else
		// 	{
		// 		StageWorld.ShowEndMenu();
		// 		AudioHandler.PlaySound(StageWorld.SoundPlayer, StageWorld.WorldSoundPaths[AudioData.WorldSounds.TimesUp], AudioData.SoundBus.Effects);
		// 	}
		// 	_silentWolfMono.Set("score_position_received_bool", false);
		// 	TryingToGetScorePosition = false;
		// }
		if (TryingToSubmitScore)
		{
			// PackedScene pnlScoresScn = (PackedScene) GD.Load("res://Interface/Panels/PnlScores.tscn");
			// PnlScores pnlScores = (PnlScores) pnlScoresScn.Instance();
			// // StageWorld.GetNode<PnlScores>("HUD/PnlScores").QueueFree();
			// pnlScores.PauseMode = PauseModeEnum.Process;
			// StageWorld.GetNode<CanvasLayer>("HUD").AddChild(pnlScores);
			// pnlScores.Name = "PnlScores";
			// pnlScores.Visible = false;
			// pnlScores.RectPosition = new Vector2(49,10);

			// StageWorld.GetNode<PnlScores>("HUD/PnlScores").Start();
			StageWorld.GetNode<Panel>("HUD/PnlHighScore").Visible = false;
			StageWorld.ShowEndMenu();
			// _silentWolfMono.Set("score_submitted_bool", false);
			TryingToSubmitScore = false;
		}
	}


}

