// CntScores: responsible for Loading scores from file, and Rendering scores into a VBoxContainer
// Intended to be used in conjunction with a ScoreHandler script which handles saving scores to file
using Godot;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;

public class CntScores : Panel
{
	// private int maxLines = 10;

	private System.Collections.Generic.List<ScoreUnit> _scoreList = new System.Collections.Generic.List<ScoreUnit>();
	private PackedScene _pnlScoreUnitScn;
	private Node _silentWolfMono;
	// private DynamicFont fontScore;

	// public int gameMode {set; get;} = 1;

	public override void _Ready()
	{   
		_pnlScoreUnitScn = (PackedScene)GD.Load("res://Stages/Scores/PnlScoreUnit.tscn");
		_silentWolfMono = GetNode("/root/SilentWolfMono");
		// Set the font we are going to use for the score labels
		// fontScore = (DynamicFont) GD.Load("res://Interface/Fonts/SayDo/SayDo20Thin.tres");
		
		// The amount of seperation (vertically) between scores
		// Set("custom_constants/separation", 40);

		// // Make a jagged array to store 4 score text elements per line of score
		// for (int i = 0; i < scoreList.Count; i++)
		// {
		//     scoreList[i] = new string[4];
		// }
	}

	public void ShowScores()
	{
		ClearRenderedScores();
		LoadAndRenderScores();
		// // TestScores();
		// ClearRenderedScores();
		// RenderScores();
	}

	public void ClearRenderedScores()
	{
		foreach (Node child in GetChildren())
			child.QueueFree();
	}

	private void RenderScores()
	{
	   // Now we are managing the position to place labels (only need to focus on horizontal, as VBox manages vertical)
		// We do not use HBox as it does not give the separation we want
		// We track a position variable and increment the horizontal position depending on the element, to keep all aligned
		Vector2 position = new Vector2(0, 0);
		float[] hPosIncrementArr = new float[6] {170,105,270,-270,270,0};


		// if (gameMode == 1)
		// {   // separation is different if showing single player scores
			hPosIncrementArr = new float[6] {170,105,270,0,0,0};

		// }


		Node silentWolfMono = GetNode("/root/SilentWolfMono");
		int maxLines = (int)silentWolfMono.Get("max_scores");

		// limit the number of lines to the maximum displayable
		double totalLines = Math.Min(maxLines, _scoreList.Count);
		// if (gameMode == 2)
		// {
		// 	totalLines = Math.Min(Math.Floor(maxLines/2.0),scoreList.Count); // if multiplayer, can show half the number of lines
		// }

		for (int i = 0; i < totalLines; i++)
		{
			// Container box = new Container();
			// AddChild(box);

			// Container box2 = new Container();

			// if (gameMode == 2)
			// {
			// 	AddChild(box2);
			// }
			Panel _pnlScoreUnit = (Panel) _pnlScoreUnitScn.Instance();
			AddChild(_pnlScoreUnit);
			// Convert the scores into an array of strings to parse more easily
			string[] currentScores = new string[3] {
				_scoreList[i].Position + ".",
				_scoreList[i].PlayerName,
				_scoreList[i].Score
			};

			for (int j = 0; j < currentScores.Length; j++)
			{
				_pnlScoreUnit.GetNode<Label>("LblPos").Text = currentScores[0];
				_pnlScoreUnit.GetNode<Label>("LblName").Text = currentScores[1];
				_pnlScoreUnit.GetNode<Label>("LblScore").Text = currentScores[2];
			}
			// position.x = 0;
			_pnlScoreUnit.RectSize  = new Vector2(660, 40);
			_pnlScoreUnit.RectPosition = position;
			position = new Vector2(position.x, position.y+41);
		}
		GetNode<AnimationPlayer>("../Anim").Play("FadeInScores"); // BAD BAD BAD
	}
// {game_id_version:The Great Egg-scape;1.0, ld_name:main, player_name:test name, score:1075, score_id:3e9067ad-4496-4e99-afa1-2a0ef1a34056, timestamp:1608308079946}
	private void LoadAndRenderScores()
	{
		_scoreList.Clear();
		Node silentWolfMono = GetNode("/root/SilentWolfMono");
		silentWolfMono.Call("load_scores");
		// await ToSignal(silentWolfMono, "scores_received");
		// string[] scores = (string[])silentWolfMono.Get("scores");
		// // GD.Print(scores);
		// for (int i = 0; i < scores.Length; i++)
		// {
		// 	string input = scores[i];
		// 	string output = Regex.Match(input, "player_name:(.*?), score:").ToString();
		// 	string playerName = output.Substring(12, output.Length-20);
		// 	// GD.Print(" name: " + playerName);
		// 	string inputScore = scores[i];
		// 	string outputScore = Regex.Match(inputScore, "score:(.*?), score_id:").ToString();
		// 	string playerScore = outputScore.Substring(6, outputScore.Length-17);
		// 	// GD.Print(" score: " + playerScore);

		// 	ScoreUnit scoreUnit = new ScoreUnit();
		// 	scoreUnit.PlayerName = playerName;
		// 	scoreUnit.Score = playerScore.ToString();
		// 	scoreUnit.Position = i+1;
		// 	_scoreList.Add(scoreUnit);
		// }

		// ClearRenderedScores();
		// RenderScores();
		
	}

	private bool _scoresReceived = false;

	public override void _Process(float delta)
	{
		base._Process(delta);
		// bool scoresReceived = (bool)_silentWolfMono.Get("scores_received_bool");

		// if (scoresReceived == true)
		// {
		// 	string[] scores = (string[])_silentWolfMono.Get("scores");
		// 	// GD.Print(scores);
		// 	for (int i = 0; i < scores.Length; i++)
		// 	{
		// 		string input = scores[i];
		// 		string output = Regex.Match(input, "player_name:(.*?), score:").ToString();
		// 		string playerName = output.Substring(12, output.Length-20);
		// 		// GD.Print(" name: " + playerName);
		// 		string inputScore = scores[i];
		// 		string outputScore = Regex.Match(inputScore, "score:(.*?), score_id:").ToString();
		// 		string playerScore = outputScore.Substring(6, outputScore.Length-17);
		// 		// GD.Print(" score: " + playerScore);

		// 		ScoreUnit scoreUnit = new ScoreUnit();
		// 		scoreUnit.PlayerName = playerName;
		// 		scoreUnit.Score = playerScore.ToString();
		// 		scoreUnit.Position = i+1;
		// 		_scoreList.Add(scoreUnit);
		// 	}

		// 	ClearRenderedScores();
		// 	RenderScores();
		// 	_silentWolfMono.Set("scores_received_bool", false);
		// }

	}


	private void OnDumbTimerTimeout()
	{		
		string[] scores = (string[])_silentWolfMono.Get("scores");
			// GD.Print(scores);
		for (int i = 0; i < scores.Length; i++)
		{
			string input = scores[i];
			string output = Regex.Match(input, "player_name:(.*?), score:").ToString();
			string playerName = output.Substring(12, output.Length-20);
			// GD.Print(" name: " + playerName);
			string inputScore = scores[i];
			string outputScore = Regex.Match(inputScore, "score:(.*?), score_id:").ToString();
			string playerScore = outputScore.Substring(6, outputScore.Length-17);
			// GD.Print(" score: " + playerScore);

			ScoreUnit scoreUnit = new ScoreUnit();
			scoreUnit.PlayerName = playerName;
			scoreUnit.Score = playerScore.ToString();
			scoreUnit.Position = i+1;
			_scoreList.Add(scoreUnit);
		}

		ClearRenderedScores();
		RenderScores();
	}

	

	public int GetScoreAtPosition(int Position)
	{
		if (_scoreList.Count < 10)
		{
			return 0;
		}
		// int posToCheck = Math.Min(Position, _scoreList.Count);
		if (!_scoreList.Contains(_scoreList.Find(x => x.Position == Position)))
		{
			return Convert.ToInt32(_scoreList.Last().Score);
		}
		return Convert.ToInt32(_scoreList.Find(x => x.Position == Position).Score);
	}

	private void LoadScores2()
	{

		// string path = "PlayerData/Scores/Scores" + gameMode + ".wpd";
		// // GD.Print(path);
		// if (! System.IO.File.Exists(path))
		// {
		// 	GD.Print("Path does not exist!");
		// 	return;
		// }
		// DataBinary scoreData = FileBinary.LoadFromFile(path);
		// scoreList = (List<ScoresUnit>)scoreData.dataDict["scoreList"];

		// ConsoleTest();
	}

	// private void ConsoleTest()
	// {
	// 	foreach (ScoresUnit unit in scoreList)
	// 	{
	// 		GD.Print(unit.Date);
	// 		GD.Print(unit.Level);
	// 		GD.Print(unit.PlayerName1);
	// 		GD.Print(unit.Score1);
	// 		GD.Print(unit.PlayerName2);
	// 		GD.Print(unit.Score2);

	// 	}
	// }

	// // For testing
	// private void TestScores()
	// {
	// 	scoreList.Clear();

	// 	Random rand = new Random();

	// 	for (int i = 0; i < maxLines; i++)
	// 	{
	// 		scoreList.Add(new ScoresUnit()
	// 		{
	// 			Date = "14/06/2014",
	// 			Level = rand.Next(20),
	// 			PlayerName1 = "Rand player",
	// 			Score1 = rand.Next(1000),
	// 			PlayerName2 = "another rand one",
	// 			Score2 = rand.Next(500)
	// 		});
	// 	}

	// 	scoreList.Sort((x,y) => (y.Score1 + y.Score2).CompareTo((x.Score1 + x.Score2)));
	// 	// it won't show in order for single player cuz the scores are added

	// }

}

// [Serializable()]
public class ScoreUnit
{
	public int Position {get; set;}
	public string PlayerName {get; set;}
	public string Score {get; set;}
	// public string Date {get; set;}
	// public int Level {get; set;}
	// public string PlayerName1 {get; set;}
	// public int Score1 {get; set;}
	// public string PlayerName2 {get; set;} = "";
	// public int Score2 {get; set;} = 0;
} // Date, player name, level reached, score, (if applicable) player 2 name, (if applicable) player 2 score
			// {DateTime.Now.ToString("d/M/yyyy"),
			// _levelGenerator.Level.ToString(),
			// GameSettings.Instance.PlayerNames[1],
			// _finalScores[0].ToString(),
			// "",
			// ""};	

