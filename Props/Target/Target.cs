using Godot;
using System;
using System.Collections.Generic;

public class Target : Area2D
{

		private Dictionary<AudioData.TargetSounds, AudioStream> _targetSounds = AudioData.LoadedSounds<AudioData.TargetSounds>(AudioData.TargetSoundPaths);
		private AudioStreamPlayer2D _soundPlayer;
		public delegate void TargetKilledSelfDelegate(Target target);
		public event TargetKilledSelfDelegate TargetKilledSelf;
		//Adjust these parameters in TargetHandler
		public int TargetID {get; set;}
		public int Score {get; set;} = 1; // set in target handler
		public int SidewaysMovement = 0;
		public Vector2 velocity;
		public int Speed = 0;
		public bool TimeBonus = false;
		AnimationPlayer anim;
		public Vector2 screensize;
		private Random _random = new Random();
		private string _deathAnimName = "Death";
		public bool EggInside {get; set;} = false;

		public override void _Ready()
		{
			_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
			Connect("body_entered", this, nameof(OnBodyEntered));
			Connect("body_exited", this, nameof(OnBodyExited));
			velocity = new Vector2();
			screensize = GetViewport().GetSize();
			Speed = _random.Next(10,50);
			anim = (AnimationPlayer)GetNode("AnimationPlayer");
			PlayAnim("Spawn");
			
		}

		public void PlaySound(AudioData.TargetSounds sound)
		{
			if (_soundPlayer.Stream == _targetSounds[sound] && _soundPlayer.Playing)
			{
				return;
			}
			AudioHandler.PlaySound(_soundPlayer, _targetSounds[sound], AudioData.SoundBus.Effects);
		}

		public void OnBodyEntered(PhysicsBody2D body)
		{
			// if (EggInside)
			// {
			// 	return;
			// }
			if (body is Egg egg)
			{
				// if (egg.Landed)
				// {
					EggInside = true;
				// }
			}
		}

		public void OnBodyExited(PhysicsBody2D body)
		{
			if (body is Egg egg)
			{
				EggInside = false;
			}
		}

		public void Die()
		{
			// TargetHit = null;
			TargetKilledSelf = null;
			PlayAnim(_deathAnimName);
			//QueueFree(); // death anim first 
		}

		public void PlayHitSound()
		{
			// PlaySound(GetRandomCheerSound());
		}

		public void PlayAnim(string animName)
		{
			anim.Play(animName);
		}

		public override void _PhysicsProcess(float delta)
		{
			velocity.x += SidewaysMovement;
			velocity.y += _random.Next(-5,5);
			if (Position.x > screensize.x + 250 || Position.x < -250 || Position.y > screensize.y + 250 || Position.y < -250)
			{
				// Die();
				TargetKilledSelf?.Invoke(this);
			}
			if (velocity.Length() > 0)
			{
				velocity = velocity.Normalized() * Speed;
			}
			Position += velocity * delta;
		
		}

		private void OnAnimFinished(String anim_name)
		{
			// Replace with function body.
			// GD.Print(anim_name);
			if (anim_name == "Death" || anim_name == "TargetDespawn")
			{
				QueueFree();
			}
			if (anim_name == "Spawn")
			{
				PlayAnim("Idle");
			}
		}

		private void OnDespawnTimerTimeout()
		{
			// GetNode<AnimationPlayer>("AnimationPlayer").RemoveAnimation("Death");
			// GetNode<AnimationPlayer>("AnimationPlayer").RenameAnimation("TargetDespawn", "Death");
			_deathAnimName = "TargetDespawn";
			TargetKilledSelf?.Invoke(this);
		}
	
}



