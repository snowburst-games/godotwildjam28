using Godot;
using System;

public class TargetTest : Area2D
{
	public int TargetID {get; set;}
	public int Score {get; set;} = 100; // set in target handler

	public bool TimeBonus {get; set;} = false;

	public void Die()
	{
		// TargetHit = null;
		QueueFree(); // death anim first 
	}
}
