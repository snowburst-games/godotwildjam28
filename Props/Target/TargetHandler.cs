using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class TargetHandler : Node2D
{
	public delegate void TargetHitDelegate(int score, Vector2 pos, bool TimeBonus);
	public event TargetHitDelegate TargetHit;
	
	public enum TargetType {EggTimer, Nest, Island}
	
	public Dictionary<TargetType, string> _targetDict = new Dictionary<TargetType, string>()
	{
		{TargetType.EggTimer, "res://Props/Target/Target.tscn"},
		{TargetType.Nest, "res://Props/Target/TargetTwo.tscn"},
		{TargetType.Island, "res://Props/Target/TargetThree.tscn"}
	};

	public enum MovementType {LeftToRight, RightToLeft, Stationary}

	public Dictionary<MovementType, Action<Target>> movementDict = new Dictionary<MovementType, Action<Target>>();

	public int _numberOfTargetsGenerated = 0;

	public List<Target> _targets = new List<Target>();

	Timer spawnTimer;

	Random random = new Random(); 

	int _yPosUpper = 250;
	int	_yPosLower = 50;
	int _xPosUpper = 900;
	int _xPosLower = 50;

	float _waitTimeIncrement = 0;

	public override void _Ready()
	{
		//START TIMER
		//WHEN TIMER WAIT TIME ENDS, GENERATE A NEW TARGET
		spawnTimer = (Timer)GetNode("SpawnTimer");
		spawnTimer.WaitTime = 3;
		spawnTimer.Start();
		movementDict.Add(MovementType.LeftToRight, MovementPatternLeftToRight);
		movementDict.Add(MovementType.RightToLeft, MovementPatternRightToLeft);
		movementDict.Add(MovementType.Stationary, MovementPatternStationary);

	//	GenerateTarget(TargetType.EggTimer, MovementPatternStationary); //replace with timer
	//	GenerateTarget(TargetType.Nest, MovementPatternStationary2);
	}
	
	public void OnSpawnTimerTimeout()
	{
		GenerateTarget((TargetType)random.Next(0,3), (MovementType)random.Next(0,3)); //still need to randomise movement patterns
		spawnTimer.WaitTime = Math.Max(1f,random.Next(1,6)-_waitTimeIncrement);
		_waitTimeIncrement += 0.05f;
		GD.Print("Wait time increment: ", _waitTimeIncrement);
		GD.Print("Wait time: ", spawnTimer.WaitTime);
		spawnTimer.Start();
	}

	public void GenerateTarget(TargetType targetType, MovementType movementType)
	{
		Random random = new Random();
		_numberOfTargetsGenerated += 1;
		PackedScene targetScn = (PackedScene)GD.Load(_targetDict[targetType]); //"res://Props/Target/TargetTest.tscn");
		Target target = (Target) targetScn.Instance();
		target.TargetKilledSelf+=this.DisposeTarget;
		target.TargetID = _numberOfTargetsGenerated; 
		target.TimeBonus = (targetType == TargetType.EggTimer);
		AddChild(target);
		movementDict[movementType](target);
		_targets.Add(target);
		target.Speed = random.Next(15,125);	
		target.SidewaysMovement = random.Next(-10,10);

		if (movementDict[movementType]== MovementPatternStationary && targetType != TargetType.EggTimer)
		{
			target.SidewaysMovement = 1;
			target.Speed = 1;
			target.GetNode<Timer>("DespawnTimer").Connect("timeout", target, "OnDespawnTimerTimeout");
			target.GetNode<Timer>("DespawnTimer").WaitTime = Math.Max(4f, random.Next(5,10)-_waitTimeIncrement);
			target.GetNode<Timer>("DespawnTimer").Start();
		}
		target.Score = Mathf.Clamp(1*target.Speed, 15, 125); 
		if (targetType == TargetType.EggTimer)
		{
			target.Speed = random.Next(50,125);	
			target.Speed += Convert.ToInt32(target.Speed*_waitTimeIncrement)/2;
			target.Score = Convert.ToInt32(Mathf.Clamp(1*target.Speed, 15, 125)/4f); 
			target.SidewaysMovement = random.Next(5,10);
			if (random.Next(0,2) == 0)
			{
				target.SidewaysMovement *= -1;
			}			
			movementDict[(MovementType)random.Next(0,2)](target);
		}
		

	}

/* 	private void GenerateTarget(TargetType targetType, Action<Target> movementType)
	{
		_numberOfTargetsGenerated += 1;
		PackedScene targetScn = (PackedScene)GD.Load(_targetDict[targetType]); //"res://Props/Target/TargetTest.tscn");
		Target target = (Target) targetScn.Instance();
		target.Score = 55; // customise target stuff after spawning 
		target.TargetID = _numberOfTargetsGenerated; 
		AddChild(target);
		// targetTest.TargetHit+=this.OnTargetHit;
		movementType(target);
		_targets.Add(target);
		// MovementPatternStationary(targetTest);
	} */



	

	public void MovementPatternLeftToRight(Target target)
	{
		target.Position = new Vector2(random.Next(_xPosLower,_xPosUpper),random.Next(_yPosLower, _yPosUpper));
	}

	public void MovementPatternRightToLeft(Target target)
	{
		target.Position = new Vector2(random.Next(_xPosLower,_xPosUpper),random.Next(_yPosLower, _yPosUpper));
	}
	
	public void MovementPatternStationary(Target target)
	{
		target.Position = new Vector2(random.Next(_xPosLower,_xPosUpper),random.Next(_yPosLower, _yPosUpper));
		
	}
	
	public void OnTargetHit(Target target, Vector2 landPosition, bool timeBonus)
	{
		TargetHit?.Invoke(target.Score, landPosition, timeBonus);
		// target.PlayHitSound();
		DisposeTarget(target);
	}

	public void DisposeTarget(Target target)
	{
		target.Die();
		_targets.Remove(target);
	}

	public void OnEggLanded(Egg egg)
	{
		
		List<Target> sortedTargets = _targets.OrderByDescending(x => x.TargetID).ToList();
		bool _landedOnTarget = false;
		foreach (Target tar in sortedTargets)
		{
			// if (tar.GetOverlappingBodies().Contains(egg))
			// {
				
			// 	GD.Print("egg landed on " + tar);
			// 	_landedOnTarget = true;
			// 	OnTargetHit(tar, egg.Position, tar.TimeBonus);
			// 	return;
			// }
			if (tar.EggInside)
			{
				GD.Print("egg landed on " + tar);
				_landedOnTarget = true;
				egg.PlaySound(egg.GetRandomCheerSound());
				OnTargetHit(tar, egg.Position, tar.TimeBonus);
				return;
			}
		}
		if (!_landedOnTarget)
		{
			// GD.Print("missed");
			egg.PlaySound(AudioData.EggSounds.Splat);
			TargetHit?.Invoke(0, egg.Position, false);
		}
	}

}

/*

	public void MovementPatternLeftToRight(Target target)
	{
		target.Speed = random.Next(10,50);
		target.Position = new Vector2(100,100);

		//target.Score = 45*target.Speed; 
		//target.Score = Mathf.Clamp(Score, 45, 150); 
	}

	public void MovementPatternRightToLeft(Target target)
	{
		target.Speed = random.Next(10,50);
		//target.Score = 45*target.Speed;
		//target.Score = Mathf.Clamp(Score, 45, 150); 
		target.SidewaysMovement -= 10;
		target._yPosUpper = 250;
		target._yPosLower = 50;
		target._xPosUpper = 900;
		target._xPosLower = 50;
		target.Position = new Vector2(random.Next(_xPosLower,_xPosUpper),random.Next(_yPosUpper,_yPosLower));
	}
	
	public void MovementPatternStationary(Target target)
	{
		//target.Score = 15; 
		target.SidewaysMovement = 0;
		target.velocity = 0;
		target.Speed = 0;
		_yPosUpper = 250;
		_yPosLower = 50;
		_xPosUpper = 900;
		_xPosLower = 50;
		target.Position = new Vector2(random.Next(_xPosLower,_xPosUpper),random.Next(_yPosUpper,_yPosLower));
	}

*/


