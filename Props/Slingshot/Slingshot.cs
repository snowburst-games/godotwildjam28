using Godot;
using System;
using System.Collections.Generic;

public class Slingshot : Node2D
{
	private Dictionary<AudioData.SlingshotSounds, AudioStream> _slingShotSounds = AudioData.LoadedSounds<AudioData.SlingshotSounds>(AudioData.SlingshotSoundPaths);
	private AudioStreamPlayer2D _soundPlayer;
	private Random _rand = new Random();

	// private bool SlingshotStretched {get; set;} = false;

	public override void _Ready()
	{
		base._Ready();
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");

	}

	public void PlaySound(AudioData.SlingshotSounds slingShotSound)
	{
		if (_soundPlayer.Stream == _slingShotSounds[slingShotSound] && _soundPlayer.Playing)
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayer, _slingShotSounds[slingShotSound], AudioData.SoundBus.Effects);
	}

	public AudioData.SlingshotSounds GetRandomShootSound()
	{
		return (new AudioData.SlingshotSounds[2] {AudioData.SlingshotSounds.SlingShoot1, AudioData.SlingshotSounds.SlingShoot2})[_rand.Next(0, 2)];
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		// if (SlingshotStretched)
		// {
		// 	PlaySound(AudioData.SlingshotSounds.Stretch);
		// }
	}

	public void OnSlingshotShot()
	{
		// SlingshotStretched = false;
		PlaySound(GetRandomShootSound());
	}

	public void OnSlingshotStretch()
	{
		// SlingshotStretched = true;
		PlaySound(AudioData.SlingshotSounds.Stretch);
	}

}
