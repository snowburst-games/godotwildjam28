using Godot;
using System;

public class Shadow : Sprite
{
	// Declare member variables here. 

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{

	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
	Vector2 lastPos = new Vector2(-1000,-1000);
	public override void _PhysicsProcess(float delta)
	{
		Vector2 eggScale = ((Sprite)GetParent().GetNode<Sprite>("Sprite")).Scale;
		Vector2 eggPos = ((Egg)GetParent()).Position;
		Scale = eggScale*0.9f;
		// if (lastPos != new Vector2(-1000,-1000))
		// {
		// 	GlobalPosition = lastPos.LinearInterpolate(eggPos, delta * 0.00001f);
		// 	lastPos = eggPos;
		// }
		// GlobalPosition = eggPos;
		// GlobalPosition = GlobalPosition.LinearInterpolate(eggPos, delta*0.05f);
	}	

	public float GetPositionByScale(float scale, float tarY)
	{
		return tarY*scale;
	}
}
