using Godot;
using System;
using System.Collections.Generic;

public class Egg : RigidBody2D
{
	public delegate void BallDraggedDelegate();
	public event BallDraggedDelegate BallDragged;
	public delegate void StoppedDragDelegate();
	public event StoppedDragDelegate StoppedDrag;
	public delegate void EggLandedDelegate(Egg egg);
	public event EggLandedDelegate EggLanded;
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.EggSounds, AudioStream> _eggSounds = AudioData.LoadedSounds<AudioData.EggSounds>(AudioData.EggSoundPaths);

	// public delegate void EggShotDelegate();
	// public event
	private int _pressed = -1;
	private int _boundary = 150;
	private float _force = 2.75f;
	public Vector2 CentrePos {get; set;}
	public Vector2 AimPos {get; set;}

	private Random _rand = new Random();
	// public bool Landed {get; set;} = false;

	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		
	}

	public override void _UnhandledInput(InputEvent ev)
	{
		base._UnhandledInput(ev);

		if (_pressed == 1) // after clicking the egg
		{
			GlobalPosition = GetGlobalMousePosition(); // egg follows mouse

			Vector2 distanceToCentre = GlobalPosition - CentrePos;
			if (distanceToCentre.Length() > _boundary) // if the egg goes beyond the boundary, set it to the boundary
			{
				GlobalPosition = distanceToCentre.Normalized() * _boundary + CentrePos;
			}
	
			OnDrag(); // tell the world to move the slingshot according to where the egg is
		}
		if (ev is InputEventMouseButton btnEv) 
		{
			if (!btnEv.IsPressed()) // if we stop clicking (i.e. release)
			{
				_pressed = -1;	// stop doing things that depend on clicking the egg
				Shoot();
			}
		}
	}

	public void PlaySound(AudioData.EggSounds eggSound)
	{
		if (_soundPlayer.Stream == _eggSounds[eggSound] && _soundPlayer.Playing)
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayer, _eggSounds[eggSound], AudioData.SoundBus.Effects);
	}

	public AudioData.EggSounds GetRandomCheerSound()
	{
		return (new AudioData.EggSounds[5] {AudioData.EggSounds.Cheer1, AudioData.EggSounds.Cheer2, AudioData.EggSounds.Cheer3, 
		AudioData.EggSounds.Cheer4, AudioData.EggSounds.Cheer5})[_rand.Next(0, 5)];
	}

	public AudioData.EggSounds GetRandomEggInAirSound()
	{
		return (new AudioData.EggSounds[2] {AudioData.EggSounds.EggInAir1, AudioData.EggSounds.EggInAir2})[_rand.Next(0, 2)];
	}

	private void OnEggInputEvent(Viewport viewport, Godot.Object ev, int shape_idx)
	{
		if (ev is InputEventMouseButton btnEv)
		{
			if (btnEv.IsPressed())
			{
				_pressed = 1;
			}
		}
		// Replace with function body.
	}

	private void OnDrag()
	{
		BallDragged?.Invoke();
	}

	public void Land()
	{
		EggLanded?.Invoke(this);
		// Landed = true;
		Mode = RigidBody2D.ModeEnum.Static;
		
	}

	private void Shoot()
	{
		StoppedDrag?.Invoke(); // resets the slingshot to origin
		Vector2 distance = AimPos - GlobalPosition;
		// GD.Print(distance);
		Vector2 impulse = distance.Normalized() * distance.Length() * _force;
		// GD.Print(impulse);
		Mode = RigidBody2D.ModeEnum.Rigid;
		ApplyImpulse(distance, impulse);
		ZAsRelative = false;
		ZIndex = 2;
		// ZIndex = -3;
		// set the targets to -4
		GetNode<AnimationPlayer>("Anim").Play("Fly");
		PlaySound(GetRandomEggInAirSound());
		// SetProcessInput(false);
		SetProcessUnhandledInput(false);
	}


	private void OnAnimFinished(String anim_name)
	{
		if (anim_name == "Fly")
		{
			Land();
			GetNode<AnimationPlayer>("Anim").Play("Die");
		}
		if (anim_name == "Die")
		{
			Die();
		}
	}

	private async void Die()
	{
		// also generate a splat sprite
		await ToSignal(_soundPlayer, "finished");
		BallDragged = null;
		StoppedDrag = null;
		QueueFree();
	}

}

