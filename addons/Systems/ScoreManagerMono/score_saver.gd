extends Panel

signal finished
signal high_score
signal no_high_score

var high_score = 0
var original_high_score_title = ""
var timeout = false

func _ready():
	visible = false
	$cnt_highscore.visible = false
	$cnt_checking.visible = false
	$cnt_highscore/lbl_status.visible = false
	original_high_score_title = $cnt_highscore/lbl_title.text

func start(score, max_scores):
	timeout = false
	high_score = score
	visible = true
	$cnt_checking.visible = true
	$cnt_checking/anim.play("Loading")
	$PnlTimeout.start_timeout(self)
	yield(SilentWolf.Scores.get_score_position(score), "sw_position_received")
	if timeout:
		return
	$PnlTimeout.stop_timeout()
	$cnt_checking.visible = false
	var position = SilentWolf.Scores.position
	if position <= max_scores:
		$cnt_highscore.visible = true
		$cnt_highscore/lbl_title.text = original_high_score_title + str(high_score) + "!"
		emit_signal("high_score")
	else:
		emit_signal("no_high_score")
		close_all()
		
func is_valid_name(name):
	if name == "":
		return false
	return true

func close_all():
	visible = false
	$cnt_highscore.visible = false
	$cnt_checking.visible = false
	$cnt_highscore/lbl_status.visible = false
	$cnt_highscore/btn_submit.disabled = false
	$cnt_highscore/lbl_title.text = original_high_score_title
	if $cnt_checking/anim.is_playing():
		$cnt_checking/anim.seek(0, true)
	if $cnt_highscore/anim.is_playing():
		$cnt_highscore/anim.seek(0, true)
	$cnt_checking/anim.stop(true)
	$cnt_highscore/anim.stop(true)
	emit_signal("finished")
	

func on_timeout():
	timeout = true

func _on_BtnSubmit_pressed():
	timeout = false
	if !is_valid_name($cnt_highscore/led_name.text):
		$cnt_highscore/lbl_status.visible = true
		$cnt_highscore/lbl_status.text = "Invalid player name"
		return
	$cnt_highscore/lbl_status.visible = false
	$cnt_highscore/btn_submit.disabled = true
	$cnt_highscore/anim.play("Loading")
	$PnlTimeout.start_timeout(self)
	yield(SilentWolf.Scores.persist_score($cnt_highscore/led_name.text, high_score), "sw_score_posted")
	if timeout:
		return
	$PnlTimeout.stop_timeout()
	close_all()

func _input(event):
	if Input.is_key_pressed(KEY_ENTER) and (event.is_pressed() and not event.is_echo()) and $cnt_highscore.visible:
		_on_BtnSubmit_pressed()
