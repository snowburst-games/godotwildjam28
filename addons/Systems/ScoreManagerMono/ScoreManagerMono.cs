// ScoreManagerMono script. This wraps a ScoreManager UI written in gdscript that utilises SilentWolf Leaderboard addon
// To use, change the config in score_unit.gd, and instance this node in a scene.
// Don't forget to customise the UI elements in this node and in pnl_scoreunit

// Can request to submit a high score - this checks the position of the high score and if it is within the maxScores..
// .. it submits the score (with loading animations that can be customised), and optionally then displays the leaderboard:
// StartScoreSaver(highScore:30, maxScores:10);
// Can just directly show the leaderboard (up to x max scores):
// ShowAndRefreshLeaderboard(maxScores:10);
// Or can refresh and show the leaderboard separately:
// RefreshLeaderboard(maxScores:10)
// ShowLeaderboard()
// To wipe the leaderboard:
// GetNode("score_manager").CallDeferred("wipe");

// Signals can be connected when this is instanced, for UI transition purposes (e.g. on high_score play victory sound, on score saver finished go to end menu)

// NOTE: this addon FAILS if the game is paused while the leaderboard is active. Instead consider removing any performance heavy nodes and/or muting sound and voice buses.

using Godot;
using System;

public class ScoreManagerMono : Node
{
	public override void _Ready()
	{
		
		//StartScoreSaver(10,10,true);
	}
	
	public void StartScoreSaver(int highScore, int maxScores)
	{
		GetNode("score_manager/score_saver").CallDeferred("start", highScore, maxScores);
	}
	
	public void ShowAndRefreshLeaderboard(int maxScores)
	{
		GetNode("score_manager/leaderboard").CallDeferred("show_and_refresh_board", maxScores);
	}

	public void ShowLeaderboard()
	{
		GetNode("score_manager/leaderboard").CallDeferred("show_board");
	}
	public void RefreshLeaderboard(int maxScores)
	{
		GetNode("score_manager/leaderboard").CallDeferred("refresh_board", maxScores);
	}


}
